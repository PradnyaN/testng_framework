package Restassured_reference;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Soap_reference {

	public static void main(String[] args) {
// step 1 Declare the base URL
		
	    RestAssured.baseURI="https://www.dataaccess.com";
// step 2 Declare the requestBody
		String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
// step 3 Trigger the API and fetch the responseBody
		//RestAssured.baseURI=BaseURI;
      String responseBody=given().header("Content-Type","text/xml;charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso")
		.then().extract().response().getBody().asString();
//step 4 print the responseBody
      System.out.println(responseBody);
// step 5 extract the responseBody parameters
      XmlPath Xml_res=new XmlPath(responseBody);
      String res_tag=Xml_res.getString("NumberToWordsResult");
      System.out.println(res_tag);

// step 6 validate the responseBody
      Assert.assertEquals(res_tag, "five hundred ");
		
	}

}
